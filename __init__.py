# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .unit_load import UnitLoadCase


def register():
    Pool.register(
        UnitLoadCase,
        module='stock_unit_load_cases', type_='model')

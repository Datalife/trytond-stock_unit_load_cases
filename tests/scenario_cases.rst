==============================
Stock Unit Load Cases Scenario
==============================

Imports::

    >>> import datetime
    >>> from trytond.tests.tools import activate_modules
    >>> from proteus import Model, Wizard
    >>> from dateutil.relativedelta import relativedelta
    >>> from decimal import Decimal
    >>> from trytond.modules.company.tests.tools import create_company, \
    ...     get_company
    >>> from trytond.modules.stock_unit_load.tests.tools \
    ...     import create_unit_load
    >>> now = datetime.datetime.now()


Install stock_unit_load_cases::

    >>> config = activate_modules('stock_unit_load_cases')

Create company::

    >>> _ = create_company()
    >>> company = get_company()

Reload the context::

    >>> User = Model.get('res.user')
    >>> Group = Model.get('res.group')
    >>> config._context = User.get_preferences(True, config.context)

Create unit load::

    >>> Unitload = Model.get('stock.unit_load')
    >>> unit_load = create_unit_load(config, do_state=True)

Add case codes::

    >>> ULCase = Model.get('stock.unit_load.case')
    >>> case1 = ULCase(unit_load=unit_load, number='001')
    >>> case1.save()
    >>> case2 = ULCase(unit_load=unit_load, number='001')
    >>> case2.save()
    Traceback (most recent call last):
        ...
    trytond.model.modelsql.SQLConstraintError: Case number must be unique per Unit load. - 
    >>> case2.number = '002'
    >>> case2.save()
    >>> case3 = ULCase(unit_load=unit_load, number='003')
    >>> case3.save()
    >>> case4 = ULCase(unit_load=unit_load, number='004')
    >>> case4.save()
    >>> case5 = ULCase(unit_load=unit_load, number='005')
    >>> case5.save()

Create another Unit load::

    >>> case = ULCase(unit_load=unit_load, number='003')
    >>> case.save()
    Traceback (most recent call last):
        ...
    trytond.exceptions.UserError: Number of cases for Unit load "5.0" cannot be greater than True. - 

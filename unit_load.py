# The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.model import ModelSQL, ModelView, fields, Unique
from trytond.pool import Pool
from trytond.exceptions import UserError
from trytond.i18n import gettext
import datetime


class UnitLoadCase(ModelSQL, ModelView):
    """Unit load case"""
    __name__ = 'stock.unit_load.case'

    unit_load = fields.Many2One('stock.unit_load', 'Unit load',
                                required=True, select=True,
                                ondelete='CASCADE')
    number = fields.Char('Number', required=True)
    date = fields.Function(fields.Date('Date'),
                           'get_date', searcher='search_date')

    @classmethod
    def __setup__(cls):
        super().__setup__()
        t = cls.__table__()
        cls._sql_constraints += [
            ('unit_load_number_uk1', Unique(t, t.unit_load, t.number),
                'stock_unit_load_cases.'
                'msg_stock_unit_load_case_unit_load_number_uk1')]

    @classmethod
    def validate(cls, records):
        unit_loads = {}
        for record in records:
            unit_loads.setdefault(record.unit_load, []).append(record.id)

        others = cls.search([
            ('unit_load', 'in', list(map(int, list(unit_loads.keys())))),
            ('id', 'not in', list(map(int, records)))])
        for other in others:
            unit_loads.setdefault(other.unit_load, []).append(other.id)

        for unit_load, cases in unit_loads.items():
            if (len(cases) > unit_load.cases_quantity
                    and cls._raise_many_cases_exception()):
                raise UserError(gettext('stock_unit_load_cases.'
                        'msg_stock_unit_load_case_many_cases',
                        unit_load=unit_load.rec_name,
                        cases_quantity=unit_load.cases_quantity))
        super(UnitLoadCase, cls).validate(records)

    @classmethod
    def _raise_many_cases_exception(cls):
        return True

    @classmethod
    def get_date(cls, cases, name):
        ul_ids = [c.unit_load.id for c in cases]
        pool = Pool()
        UnitLoad = pool.get('stock.unit_load')
        uls = UnitLoad.browse(ul_ids)
        uls_dates = {ul.id: ul.end_date.date() for ul in uls}
        return {c.id: uls_dates.get(c.unit_load.id, None) for c in cases}

    @fields.depends('unit_load')
    def on_change_unit_load(self):
        if self.unit_load:
            self.date = self.unit_load.end_date.date()

    @classmethod
    def search_date(cls, name, clause):
        operand1 = datetime.datetime.combine(clause[2],
                                             datetime.datetime.min.time())
        operand2 = (datetime.datetime.combine(clause[2],
                                              datetime.datetime.min.time()) +
                    datetime.timedelta(days=1))
        if clause[1] == '=':
            return [('unit_load.end_date', '>=', operand1),
                    ('unit_load.end_date', '<', operand2)]
        elif clause[1] == '>=':
            return [('unit_load.end_date', '>=', operand1)]
        elif clause[1] == '>':
            return [('unit_load.end_date', '>=', operand2)]
        elif clause[1] == '<':
            return [('unit_load.end_date', '<', operand1)]
        elif clause[1] == '<=':
            return [('unit_load.end_date', '<', operand2)]
